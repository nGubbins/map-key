﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

/// <summary>
/// Serializable class to be used in script to control loading an unloading.
/// Can use different object keys.
/// </summary>

namespace Empire
{
    [System.Serializable]
    public class MapLoader
    {
        public Vector3 Centre = Vector3.zero;

        //TODO: Load Effects
        bool UseLoadFX = false;

        public List<KeyData> KeyList = new List<KeyData>(); //TODO: provide option to make SO with re-orderable list of key data
        public GameObject defaultCube;

        //Loads level by name from directory path
        public void Load(string path, string levelName, string fileType)
        {
            path += "/" + levelName + fileType;
            StreamReader reader = new StreamReader(path);

            string line = reader.ReadLine();
            Vector3 spawnPos = Centre;
            GameObject spawnObj;

            //Loop until no more lines in file
            //Increase y by default block size for each line
            //Increase x by default block size for each character
            while(line != null)
            {
                spawnPos.x = 0;

                foreach(char key in line)
                {
                    //needs object key to check 
                    KeyData newKeyData = CheckKey(key);

                    if(newKeyData != null)
                        spawnObj = newKeyData.Object;
                    else
                    {
                        spawnObj = null;
                    }

                    if (spawnObj != null)
                    {
                        //height
                        if(newKeyData.height > 0)
                        {
                            //Spawn Default Underneath
                            for(int it = 0; it < newKeyData.height; ++it)
                            {
                                GameObject.Instantiate(defaultCube, spawnPos, Quaternion.identity);
                                ++spawnPos.y;
                            }

                            //Spawn new obj
                            GameObject.Instantiate(spawnObj, spawnPos, Quaternion.identity);
                            //Reset Spawn Height to centre
                            spawnPos.y = Centre.y;
                        }
                        else
                        {
                            GameObject.Instantiate(spawnObj, spawnPos, Quaternion.identity);
                        }
                    }
                    else
                    {
                        
                    }
                    ++spawnPos.x;
                }
                line = reader.ReadLine();
                --spawnPos.z;


            }

            Debug.Log("Loaded");
        }

        //effects: flower : spawn into zero and then move to position

        //Destroys currently loaded objects
        public void Unload(List<GameObject> curLevelObjects)
        {
            if(UseLoadFX)
            {

            }
            else
            {
                foreach(GameObject obj in curLevelObjects)
                {
                    GameObject.Destroy(obj);
                }
            }
        }

        //Saves currently loaded objects level by name from directory path
        public void Save(string path, List<GameObject> curLevelObjects)
        {
           // StreamWriter writer = new StreamWriter(path);
        }

        KeyData CheckKey(char key)
        {
            KeyData newKeyData = null;

            foreach(KeyData data in KeyList)
            {
                if(data.Character == key)
                {
                    newKeyData = data;
                    break;
                }
            }

            if(newKeyData == null)
            {
               // Debug.LogError("Unrecognised Character in Level File: " + key);
            }

            return newKeyData;
        }
       
    }
}
