﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Serializable class that stores data used by the object key.
/// </summary>

namespace Empire
{
    public enum Type {GroundBlock, OverGround, Corner, Edge, Start, End};

    [System.Serializable]
    public class KeyData {

        public string name = "defaultBlock";
        public Type ObjType = Type.GroundBlock;
        public char Character;
        public GameObject Object;
        public int height = 0;
        
    }
}
