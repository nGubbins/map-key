﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//MonoBehaviour that controls all Empire Map Maker Info

namespace Empire
{

    public class MapManager : MonoBehaviour
    {
        public MapLoader Loader;
        public string Path = "Assets/SampleLevels";
        public string LevelName = "Level1";
        public string FileType = ".txt";
        
        // Use this for initialization
        void Start()
        {
            Loader.Load(Path, LevelName, FileType);
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
